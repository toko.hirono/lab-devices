import numpy as np

def load_mso(fname):
    dat = open(fname).readlines()
    tmp = dat[1].split(";")
    next_data = ''
    for i, t in enumerate(tmp[:-1]):
        now = next_data
        if t == '"s"':
            next_data = 'XINCR'
        elif now == 'XINCR':
            next_data = 'XZERO'
            XINCR = float(t)
        elif now == 'XZERO':
            XZERO = float(t)
            next_data = ''
        elif t == '"V"':
            next_data = 'YMULT'
        elif now == 'YMULT':
            next_data = 'YOFF'
            YMULT = float(t)
        elif now == 'YOFF':
            YOFF = float(t)
            next_data = 'YZERO'
        elif now == 'YZERO':
            YZERO = float(t)
            next_data = ''
        else:
            next_data = ''
    y = np.array(tmp[-1][:-1].split(','), dtype=int)
    # print(len(y), XINCR, XZERO, YMULT, YOFF, YZERO)
    y = (y - YOFF) * YMULT + YZERO
    x = np.arange(XZERO, XZERO + XINCR * (len(y) + 1), XINCR)[:len(y)]
    return x, y


def e2plgen(e):
    a = 0.051023722530250684
    b = 0.013189965291250544
    fe_ampout = 0.07450071206162819
    return ((fe_ampout / 1640 * e) - b) / a


def plgen2e(pls):
    a = 0.051023722530250684
    b = 0.013189965291250544
    fe_ampout = 0.07450071206162819
    return 1640 / fe_ampout * (a * pls + b)
