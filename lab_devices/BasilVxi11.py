#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, KIT
# ------------------------------------------------------------
#
import logging

import vxi11

from basil.TL.TransferLayer import TransferLayer

logger = logging.getLogger(__name__)


class BasilVxi11(TransferLayer):
    '''Transfer layer of serial device using the vxi11 module.
    '''

    def __init__(self, conf):
        super(BasilVxi11, self).__init__(conf)
        self._port = None

    def init(self):
        '''
        connect to the instrument
        '''
        super(BasilVxi11, self).init()
        self.address = self._init.get('address', None)
        try:
            self.read_termination = bytes(self.address, 'utf-8')
        except TypeError as e:
            logger.debug(e)
        self.timeout = self._init.get('timeout', None)  # TODO figureout how to set timeout
        self._port = vxi11.Instrument(self.address)
        #self._port = serial.Serial(**{key: value for key, value in self._init.items() if key not in ("read_termination", "write_termination")})

    def close(self):
        super(BasilVxi11, self).close()
        self._port.close()

    def write(self, data):
        logger.debug('write', data)
        self._port.write(data)

    def read(self):
        return self._port.read()

    def query(self, data):
        return self._port.ask(data)
