
import os
import time
import logging
import tables as tb
import numpy as np
from basil.dut import Dut


logger = logging.getLogger()


OUTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + os.sep + 'output'


def mk_fname(ext="data.npy", dirname=None, outdir=OUTDIR):
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(outdir, prefix)
    if not os.path.exists(dirname):
        os.system("mkdir -p {0:s}".format(dirname))
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)


class Smu(Dut):
    def __init__(self, conf=None, **kwargs):
        address = kwargs.pop('address', '192.168.10.101')
        if conf is None:
            conf = '''
transfer_layer:
  - name : socket
    # type : Socket
    type : lab_devices.AsciiSocket  # Socket in basil is corrupted.
    init :
      address: {0:s}
      port: 5025
      write_termination: "\\n"
      timeout: 5

hw_drivers:
  - name: smu
    type: scpi
    interface: socket
    init:
        device: ../../../lab-devices/lab_devices/keysightb2901a   # relative path from basil
            '''.format(address)

        super(Smu, self).__init__(conf=conf)

    def reconnect(self):
        self['socket'].close()
        self['socket'].init()

    def set_voltage(self, vol):
        self['smu'].set_voltage(vol)
        logger.info('set_voltage:vol={0:0.3f}V'.format(vol))

    def get_values(self):
        ret = self['smu'].get_measure()
        ret = np.array(ret.split(','), dtype='f')
        #if ret == 9.91E37:
        #    logger.warn('get_values: volt = NaN')
        #    ret = np.nan
        return ret

    def get_voltage(self):
        try:
            ret = self['smu'].get_measure()
        except TimeoutError:
            logger.warn('not responding, reconnect first')
        ret = float(ret.split(',')[0])
        if ret == 9.91E37:
            logger.warn('get_values: volt = NaN')
            ret = np.nan
        return ret

    def get_current(self):
        try:
            ret = self['smu'].get_measure()
        except TimeoutError:
            logger.warn('not responding, reconnect first')
        return float(ret.split(',')[1])  # TODO warn for nvalid data

    def iv_curve(self, volts, ave_n=10):
        fname = mk_fname('iv.h5')
        iv_dtype = [('step', '<i4'), ('volt', '<f4'), ('curr', '<f4'),
                    ('res', '<f4'), ('time', '<f4'), ('stat', '<f4'), ('sour', '<f4')]
        iv_dtype = np.dtype(iv_dtype)
        logger.info('iv_curve:fname={}'.format(fname))
        with tb.open_file(fname, 'w') as f:
            iv_tbl = f.create_table(
                f.root,
                description=iv_dtype,
                title='iv',
                name='iv',
                filters=tb.Filters(complib='zlib', complevel=5, fletcher32=False))
            iv_tbl = f.root.iv
            for i, v in enumerate(volts):
                self['smu'].set_voltage(v)
                time.sleep(2)
                for _ in range(ave_n):
                    time.sleep(0.1)
                    ret = self['smu'].get_measure()
                    iv_tbl.row['step'] = i
                    ret = np.array(ret.split(','), dtype='<f4')
                    logger.info(i, ret)
                    for ii, cname in enumerate(iv_dtype.names[1:]):
                        iv_tbl.row[cname] = ret[ii]
                    iv_tbl.row.append()
                    iv_tbl.flush()
                if np.abs(ret[0] - ret[-1]) > 2.0:
                    break
        for vv in np.arange(v, 0, 5):
            self['smu'].set_voltage(vv)
            time.sleep(0.5)
        self['smu'].set_voltage(0)
        logger.info('iv_curve:fname={} DONE'.format(fname))


class PlsGen(Dut):
    def __init__(self, conf=None, **kwargs):
        if conf is None:
            conf = '''
transfer_layer:
  - name : BasilVxi11
    type : lab_devices.BasilVxi11
    init :
      address: 192.168.10.104

hw_drivers:
  - name: afg3252c
    type: scpi
    interface: BasilVxi11
    init:
        device: ../../../lab-devices/lab_devices/afg3252c  # relative path from basil
            '''
        super(PlsGen, self).__init__(conf=conf)
        self.SET = {}

    def init(self):
        super(PlsGen, self).init()
        high, low = self.get_inj_amp()
        self.SET['high'] = high
        self.SET['low'] = low

    def set_inj_phase(self, inj_phase):
        self['afg3252c'].set_delay(inj_phase, channel=1)
        logger.info('afg3252 set_phase delay={0:.3g}'.format(inj_phase))

    def set_inj_amp(self, high, unit='V', low=None, channel=1):
        if high < self.SET['low'] + 0.1:
            logger.warn('set_inj_amp: high cannot be {0:.3f}V. it will be forced to 0.1+{1:.3f}V'.format(high, self.SET['low']))
            high = self.SET['low'] + 0.1
        self['afg3252c'].set_voltage_high('{0:.3f}{1}'.format(high, unit), channel=channel)
        self.SET['high'] = high
        logger.info('afg3252 set_amp high={0:.3f}'.format(high))
        if low is not None:
            self['afg3252c'].set_voltage_low('{0:.3f}{1}'.format(low, unit), channel=channel)
            self.SET['low'] = low
            logger.info('afg3252 set_amp low={0:.3f}'.format(low))
        time.sleep(0.15)  # TODO check "ready" status

    def get_inj_amp(self, channel=1):
        high = float(self['afg3252c'].get_voltage_high(channel=channel))
        low = float(self['afg3252c'].get_voltage_low(channel=channel))
        return high, low

    def get_configuration(self, channel=1):
        res = super(PlsGen, self).get_configuration()
        res['status'] = {}
        res['status']['delay'] = float(self['afg3252c'].get_delay(channel=channel))
        res['status']['width'] = float(self['afg3252c'].get_width(channel=channel))
        res['status']['period'] = float(self['afg3252c'].get_period(channel=channel))
        res['status']['cycle'] = int(float(self['afg3252c'].get_burst_cycles(channel=channel)))
        res['status']['high'] = float(self['afg3252c'].get_voltage_high(channel=channel))
        res['status']['low'] = float(self['afg3252c'].get_voltage_low(channel=channel))
        return res

    def e2plgen(e):
        a = 0.051023722530250684
        b = 0.013189965291250544
        fe_ampout = 0.07450071206162819
        return ((fe_ampout / 1640 * e) - b) / a

    def plgen2e(pls):
        a = 0.051023722530250684
        b = 0.013189965291250544
        fe_ampout = 0.07450071206162819
        return 1640 / fe_ampout * (a * pls + b)
