import socket
import time
import logging

logger = logging.getLogger()

from basil.TL.TransferLayer import TransferLayer


class AsciiSocket(TransferLayer):
    '''
        General direct socket TL implementation.
        Used for TCP/IP based direct communication with devices.
        Commands are handled including encoding, read and write termination.
    '''

    def __init__(self, conf):
        super(AsciiSocket, self).__init__(conf)

    def init(self):
        '''
            Create socket object and connect to specified ip address and port.
        '''
        super(AsciiSocket, self).init()
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.encoding = self._init.get('encoding', 'ascii')
        self.write_termination = self._init.get('write_termination', '\n')  # in yaml file, LF is "\\n" with 2 back-slash
        self.read_termination = self._init.get('read_termination', self.write_termination)
        self.query_delay = self._init.get('query_delay', 0)
        address = self._init.get('address')
        port = self._init.get('port')
        timeout = self._init.get('timeout', None)

        self._sock.connect((address, port))
        if timeout is not None:
            self._sock.settimeout(timeout)

    def close(self):
        super(AsciiSocket, self).close()
        self._sock.close()

    def write(self, data):
        if type(data) == bytes:
            cmd = data + self.write_termination.encode(self.encoding)
        else:
            cmd = (data + self.write_termination).encode(self.encoding)
        logger.debug('write cmd={}'.format(cmd))
        self._sock.send(cmd)

    def read(self, buffer_size=1024):
        ret = ''
        for loop_i in range(100):
            time.sleep(self.query_delay)
            try:
                r = self._sock.recv(buffer_size).decode('ascii')
            except TimeoutError:
                logger.error('sock.recv timeout sock={}'.format(self._sock))
                return None
            logger.debug('read ret={}'.format(r))
            if r[-len(self.read_termination):] == self.read_termination:
                return ret + r[:-len(self.read_termination)]
            else:
                ret = ret + r
        if loop_i == 100 - 1:
            logger.debug('too many data loop_i={} buffer_size={} len_ret={}'.format(
                loop_i, buffer_size, len(ret)))
        return ret

    def query(self, data, buffer_size=1):
        self.write(data)
        return self.read(buffer_size)